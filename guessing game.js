main();

function main() {
    var guessCount = generateGuess();
    displayResult(guessCount);
}

function displayResult(guessCount) {
  guessCount = "It took me " + guessCount + " guesses";
  document.getElementById("guessCount").innerHTML = guessCount;

}

function generateGuess() {
    var guessCount = 0;
    var highLow;
    var max = 100;
    var min = 0;
    var guess = (min + max) / 2;
    do {
        var guess = Math.round(guess);
        var min = Math.round(min);
        var max = Math.round(max);
        document.getElementById("guess").innerHTML = guess;
        guessCount = guessCount + 1;
        highLow = getHighLow();
        if (highLow == "l" || highLow == "L") {
            min = guess + 1;
        }
        if (highLow == "h" || highLow == "H") {
            max = guess - 1;
        }
        guess = (min + max) / 2;
    } while (highLow == "h" || highLow == "H" || highLow == "l" || highLow == "L");

    return guessCount;
}

function getHighLow() {
    var highLow = document.getElementById('highLow').value
    return highLow;
}