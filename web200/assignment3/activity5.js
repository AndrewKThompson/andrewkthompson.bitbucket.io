//Create a program that calculates the area of a room to determine the amount of floor covering required. 
//The room is rectangular with the dimensions measured in feet with decimal fractions. 
//The output needs to be in square yards. 
//There are 3 linear feet to a yard.
// Andrew Thompson
main();

function main() {
    var width = getWidth();
    var len = getLength();
    var area = calcArea(len, width);
    var yds = calcSquareYards(area);
    displayResults(yds);
}

function getWidth() {
    var width = prompt("Width of the room in feet?");
    return width;
}

function getLength() {
    var len = prompt("Lengthh of the room in feet?");
    return len;
}

function calcArea(len, width) {
    var area = len * width;
    return area;
}

function calcSquareYards(area) {
    var yds = area / 9;
    yds = yds.toFixed(2);
    //ref: https://stackoverflow.com/questions/45381590/js-limit-the-number-of-decimal-places-in-a-number
    return yds;
}

function displayResults(yds) {
    var ydsResults = "The area of your room is " + yds + " square yards."
   document.getElementById("ydsResults").innerHTML = ydsResults;
}