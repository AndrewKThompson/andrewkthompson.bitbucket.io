//Create a web page that allows the user to enter book information for a bibliography. Include properties for title, author, year, publisher, city, and state.
// Both APA and MLA format (expansion to both covers two acitivites)
//Andrew Thompson
//Ref: https://www.w3schools.com/js/js_object_definition.asp , https://www.w3schools.com/jsref/met_document_getelementsbyclassname.asp , https://stackoverflow.com/questions/1533568/what-is-the-correct-way-to-write-html-using-javascript

document.getElementById("submit").onclick = main;
 
function main(){
    var title = document.getElementById("title").value;
    var lastName = document.getElementById("lastName").value;
    var firstName = document.getElementById("firstName").value;
    var middleInitial = document.getElementById("middleInitial").value;
    var year = document.getElementById("year").value;
    var city = document.getElementById("city").value;
    var state = document.getElementById("state").value;
    var publisher = document.getElementById("publisher").value;
    var book = new Object();
    book.title = title;
    book.lastName = lastName;
    book.firstName = firstName;
    book.middleInitial = middleInitial;
    book.year = year;
    book.city = city;
    book.state = state;
    book.publisher = publisher
    book.apa = apa(book);
    book.mla = mla(book);
    outputResult(book);
}

function apa(book) {
    var fi = book.firstName;
    fi = fi.substring(0);
    book.apa = book.lastName + ", " + fi + " " + book.middleInitial + " (" + book.year + ") " + book.title + ". " + book.city + ", " + book.state + ": " + book.publisher;
    return book.apa
}

function mla(book) {
    var fi = book.firstName;
    fi = fi.substring(0);
    book.mla = book.lastName + ", " + book.firstName + " " + book.middleInitial + ". " + book.title + ". " + book.publisher + ", " + book.year;
    return book.mla;
}

function outputResult(book) {
    document.getElementById("apaResult").innerHTML = book.apa
    document.getElementById("mlaResult").innerHTML = book.mla

}