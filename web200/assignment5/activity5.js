// Activity 5: 
// Create a program that helps the user determine what sock size to order based on their shoe size:
// < 4 = extra small
// 4 to 6 = small
// 7 to 9 = medium
// 10 to 12 = large
// 13+ = extra large

//Input vlaidation acitivty 1: program updated to validate input. 

document.getElementById('activity5').onclick = main;


function main() {
    var shoeSize =  Number(document.getElementById('shoeSize').value);
    validateShoeSize(shoeSize);
    displayResult(sockSize);
}

function validateShoeSize(shoeSize) {
    if (shoeSize + 0 != shoeSize) {
    document.getElementById("error").innerHTML = "please enter a number";
    }
    else  var sockSize = getSockSize(shoeSize);
    return sockSize;
}

function getSockSize(shoeSize) {
    if (shoeSize <= 0 || shoeSize =="") {
        document.getElementById("error").innerHTML = "error: please enter your shoe size";
    }
    else if (shoeSize < 4) {
        sockSize = "Extra Small"
    } else if (shoeSize >= 4 && shoeSize <= 6) {
        sockSize = "Small"
    } else if (shoeSize >= 7 && shoeSize <= 9) {
        sockSize = "Medium"
    } else if (shoeSize >= 10 && shoeSize <= 12) {
        sockSize = "large"
    } else if (shoeSize >= 13) {
        sockSize = "Extra Large"
    }
    return sockSize;
}

function displayResult(sockSize) {
    document.getElementById("sockSize").innerHTML = sockSize;
}


