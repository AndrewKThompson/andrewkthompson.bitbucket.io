"use strict";
window.addEventListener("load", function () {
    displayWindow();
    displayScreen();
    displayLocation();
  });

  window.addEventListener("resize", function () {
    displayWindow();
  });

function displayWindow() {
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    document.getElementById("windowWidth").innerHTML = "width: " + windowWidth;
    document.getElementById("windowHeight").innerHTML = "height: " + windowHeight;
  }

  function displayScreen() {
    var screenWidth = screen.width;
    var screenHeight = screen.height;
    document.getElementById("screenWidth").innerHTML = "width: " + screenWidth;
    document.getElementById("screenHeight").innerHTML = "height: " + screenHeight;
  }

  function displayLocation() {
    var hostname = location.hostname;
    var href = location.href;
    document.getElementById("hostName").innerHTML = "host name: " + hostname;
    document.getElementById("href").innerHTML = "href: " + href;
  }


