//ref: https://stackoverflow.com/questions/222841/most-efficient-way-to-convert-an-htmlcollection-to-an-array
"use strict";
window.addEventListener("load", function () {
    displayElements("*");
  });
  
  function displayElements() {
    let result = "";
    let elements = document.getElementsByTagName("*");
    for (let i = 0; i < elements.length; i++) {
      let element = elements[i];
      element = element.toString();
      var start = element.indexOf("HTML");
     var end = element.indexOf("Element");
      element = element.substring(start + 4, end);
      console.log(element);
      result += element;
      if (i < elements.length - 1){
        result += ", "
      }
      
    }

    document.getElementById("tagNames").innerHTML = result;
  }
