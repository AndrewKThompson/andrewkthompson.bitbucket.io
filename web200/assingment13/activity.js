// This program demonstrates JavaScript form validation.
//
// References:
//   https://en.wikibooks.org/wiki/JavaScript
//act 1: creating form, act 2: validating fields.

"use strict";

window.addEventListener("load", function () {
    let elements = document.getElementsByTagName("input");

    for (let i = 0; i < elements.length; i++) {
        elements[i].addEventListener("focus", inputFocus);
        elements[i].addEventListener("input", inputInput);
    }

    document.getElementById("get").addEventListener("click", getClick);
    document.getElementById("post").addEventListener("click", postClick);

    document.getElementById("number").focus();
});

function inputFocus() {
    document.activeElement.select();
    displayPrompt();
    checkButtons();
}

function displayPrompt(id = null) {
    const prompts = {
        fname: "Enter your first name.",
        lname: "enter your last name",
        address: "Enter your address",
        city: "Enter your city",
        reigon: "Enter your reigon",
        postal: "Enter your 5 digit postal code",
        email: "Enter your email address",
        phone: "Enter your phone as ###-###-####"
    }

    let elements = document.getElementsByTagName("output");

    if (id == null) {
        id = document.activeElement.id;

        for (let i = 0; i < elements.length; i++) {
            elements[i].innerText = "";
        }
    }

    try {
        document.getElementById(id + "-prompt").innerText = prompts[id];
    } catch {
        // ignore when the active element is a button
    }
}

function inputInput() {
    let element = document.activeElement;
    let id = element.id;

    if (id == "pattern") {
        checkPattern();
    }

    if (!element.checkValidity()) {
        document.getElementById(id + "-prompt").innerText = element.validationMessage;
    } else {
        document.getElementById(id + "-prompt").innerText = "";
    }

    checkButtons();
}
// 123-56-789! VS. 123-567-89!@
function checkPattern() {
    let element = document.getElementById("pattern")
    let value = element.value;

    if (value.length > 3 && value.substr(3, 1) != "-") {
        value = value.substr(0, 3) + "-" + value.substr(3);
    }

    if (value.length > 7 && value.substr(7, 1) != "-") {
        value = value.substr(0, 7) + "-" + value.substr(7);
    }

    if (element.value != value) {
        element.value = value;
    }
}

function checkButtons() {
    let elements = document.getElementsByTagName("input");

    for (let i = 0; i < elements.length; i++) {
        if (!elements[i].checkValidity()) {
            document.getElementById("get").disabled = true;
            document.getElementById("post").disabled = true;
            return;
        }
    }

    document.getElementById("get").disabled = false;
    document.getElementById("post").disabled = false;
}

function getClick() {
    let form = document.getElementById("form")
    form.action = "http://postman-echo.com/get";
    form.method = "GET";
    form.submit();
}

function postClick() {
    let form = document.getElementById("form")
    form.action = "http://postman-echo.com/post";
    form.method = "POST";
    form.submit();
}