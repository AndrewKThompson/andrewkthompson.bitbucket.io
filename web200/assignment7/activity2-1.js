// ref: (for nicer formatting)  https://stackoverflow.com/questions/41465569/multiplication-table-in-javascript
window.addEventListener("load", function () {
    document.getElementById("start").addEventListener("input", tableInput);
    document.getElementById("end").addEventListener("input", tableInput);
});

function tableInput() {
    let start = document.getElementById("start").value;
    let end = document.getElementById("end").value;
    if (checkTableInput()) {
        document.getElementById("error1").innerText = "";
        displayTable();
    }
}

function checkTableInput() {
    let start = Number(document.getElementById("start").value);
    let end = Number(document.getElementById("end").value);
    if (isNaN(start) || isNaN(end)) {
        document.getElementById("error1").innerText =
         " please enter a number";
        return false;
    }
    return true;  
}


function displayTable() {
    let start = Number(document.getElementById("start").value);
    let end = Number(document.getElementById("end").value);

    if (end <= start) {
        document.getElementById("error1").innerText = "End must be greater than start."
        document.getElementById("multTable").innerText = "";
        return;
    }
    var result = writeResult(start, end);
    document.getElementById("multTable").innerText = result;
}

function writeResult(start, end) {
    result = ' x   ';
    start = Number(start) - 1;
    end = Number(end) + 1;
    function buff(val) {
        var buff = '';
        var pad = 4 - val;
        while (pad-- > 0)
            buff += ' ';
        return buff;
    }
    for (var i = start; i < end; i++) {       
        for (var j = start; j < end; j++) {
            if (i == start && j > start) {
                result += '[' + j + ']' + buff((j + '').length + 2);
            } else if (j == start && i > start) {
                result += '[' + i + ']';
            } else if (i > start && j > start) {
                result += buff((i * j + '').length) + i * j;
            }
        }
        result += '\n'
    }
    return result;
}
//using nested for loop