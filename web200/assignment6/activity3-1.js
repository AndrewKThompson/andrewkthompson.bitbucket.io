

document.getElementById('submit').onclick = handleInputGG;
    
 var guessCount = [];
 var min = 0;
 var max = 100;
 var guess = 50;

function handleInputGG() {
   if (checkInputGG()) {
        document.getElementById("error").innerText = "";
       displayResultsGG(guessCount, min, max, guess); 
    }
}

function checkInputGG() {
    let highLow = document.getElementById("highLow").value;
    if (highLow.trim().length != 1 ) {
        document.getElementById("error").innerText = " You must enter h, l, or e";
        return false;
    }  

    return true;
}

function displayResultsGG() {
    guessCount = Number(guessCount.length);
    guessCount.push("g")
    let highLow = document.getElementById("highLow").value;
    
    if (highLow == "h" || highLow == "H") {
        max = Number(guess) - 1;
    }

    if (highLow == "l" || highLow == "L") {
        min = Number(guess) + 1;
    }
    if (highLow == "e" || highLow == "E") {
        document.getElementById("guessCount").innerText = "It took me " + guessCount + " tries"
     }
   
   guess = Math.round((min + max) / 2);
   let guessResult = "";
   guessResult = guess.toString();
 document.getElementById("guess").innerText = guessResult;
}

