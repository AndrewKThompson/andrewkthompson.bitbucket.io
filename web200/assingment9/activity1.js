//Displays current date and time, updated every second. 
//Andrew K. Thompson
//ref: https://www.w3schools.com/jsref/jsref_getmonth.asp 

window.setInterval(dateParts, 1000);
function dateParts() {
    var d = new Date();
    var year = d.getFullYear();
    var month = getMonth(d);
    var day = d.getDate();
    var hour = d.getHours();
    var minute = d.getMinutes();
    var second = d.getSeconds();
    displayDate(year, month, day, hour, minute, second);
}

function getMonth(d) {
    var m = new Array();
    m[0] = "January";
    m[1] = "February";
    m[2] = "March";
    m[3] = "April";
    m[4] = "May";
    m[5] = "June";
    m[6] = "July";
    m[7] = "August";
    m[8] = "September";
    m[9] = "October";
    m[10] = "November";
    m[11] = "December";
    var month = m[d.getMonth()];
    return month;
}

function displayDate(year, month, day, hour, minute, second) {
    document.getElementById("year").innerHTML = "Year: " + year;
    document.getElementById("month").innerHTML = "Month: " + month;
    document.getElementById("day").innerHTML = "Day: " + day;
    document.getElementById("hour").innerHTML = "Hour: " + hour;
    document.getElementById("minute").innerHTML = "Minute: " + minute;
    document.getElementById("second").innerHTML = "Second: " + second;
}




