//Takes input from a date picker and displays the compnenets of date seperatly.
//Andrew K. Thompson
//Web 200
window.addEventListener("load", function () {
    document.getElementById("dateInput").addEventListener("input", dateInput);
  });

  function dateInput(){
    var userDate = document.getElementById("dateInput").value;
    var firstDash = userDate.indexOf("-");
    var secondDash = userDate.lastIndexOf("-");
    var end = userDate.length;
    var userYear = userDate.substring(0, firstDash);
    var userMonth = userDate.substring(firstDash + 1, secondDash);
    var userDay= userDate.substring(secondDash + 1, end);
    document.getElementById("userYear").innerHTML = "Year: " + userYear;
    document.getElementById("userMonth").innerHTML = "Month: " + userMonth;
    document.getElementById("userDay").innerHTML = "Day: " + userDay;
  }