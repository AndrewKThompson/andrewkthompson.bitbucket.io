document.getElementById("newList").onclick = newParagraph;

function newParagraph() {
    // This creates a heading.
    var elementH = document.createElement("h2");
    var main = document.getElementById("main");
    main.appendChild(elementH);
    var textH = document.createTextNode("New Paragraph");
    elementH.appendChild(textH);
    // THis creates a paragraph.
    var element = document.createElement("p");
    var text = document.createTextNode("Here's your new paragraph Sir.");
    element.appendChild(text);
    main.appendChild(element);
}