// array activity updated to generate output element with dynamic HTML
window.addEventListener("load", function () {
    document.getElementById("submit").addEventListener("click", handleInputGG);
})
var guessCount = [];
var min = 0;
var max = 100;
var guess = 50;

function handleInputGG() {

    if (checkInputGG()) {
        document.getElementById("error").innerText = "";
        displayResultsGG(guessCount, min, max, guess);
    }
}

function checkInputGG() {
    let highLow = document.getElementById("highLow").value;
    if (highLow.trim().length != 1) {
        document.getElementById("error").innerText = " You must enter h, l, or e";
        return false;
    }

    return true;
}

function displayResultsGG() {
    guessCount = Number(guessCount.length);
    guessCount++
    let highLow = document.getElementById("highLow").value;

    if (highLow == "h" || highLow == "H") {
        max = Number(guess) - 1;
    }

    if (highLow == "l" || highLow == "L") {
        min = Number(guess) + 1;
    }
    if (highLow == "e" || highLow == "E") {
        document.getElementById("guessCount").innerText = "It took me " + guessCount + " tries"
    }

    guess = Math.round((min + max) / 2);
    let guessResult = "";
    guessResult = guess.toString();
    writeOutput(guessResult);
}

function writeOutput(guessResult) {
    var actTwo = document.getElementById("act2");
    var paragraph = document.createElement("p");
    var text = document.createTextNode(guessResult);
    paragraph.appendChild(text);
    actTwo.appendChild(paragraph);
}