//Updates lesson 6 activity by using an array to store and display results. 
// lesson 12: updates again to generate html elements from javascript (the li's)
//ref:https://www.w3schools.com/js/js_arrays.asp

window.addEventListener("load", function () {
  document.getElementById("value").addEventListener("input", inputInput);
  document.getElementById("numOfExpressions").addEventListener("input", inputInput);

});

function inputInput() {
  let value = document.activeElement.value;

  if (checkInput()) {
    document.getElementById("error1").innerText = "";
    displayList();
  }
}

function checkInput() {
  let value = document.activeElement.value;
  if (isNaN(value) || value.trim().length == 0) {
    document.getElementById("error1").innerText =
      document.activeElement.id + " please enter a number";
    return false;
  }

  value = document.getElementById("numOfExpressions").value;
  if (isNaN(value) || value.trim().length == 0) {
    return false;
  }

  value = document.getElementById("value").value;
  if (isNaN(value) || value.trim().length == 0) {
    return false;
  }


  return true;
}

function displayList() {

  let numOfExpressions = Number(document.getElementById("numOfExpressions").value);
  let value = Number(document.getElementById("value").value);

  if (numOfExpressions <= 0) {
    document.getElementById("error1").innerText = "Number of expressions must be at least 1"
    document.getElementById("list").innerText = "";
    return;
  }

  let result = ""
  var operand = 1;
  var answer = 1;
  var array = [];

  while (operand <= numOfExpressions) {
    answer = value * operand;
    array.push(value + " * " + operand + " = " + answer);
    operand++
  }

  let unorderedList = document.getElementById("unorderedList");
  
  while (unorderedList.lastChild) {
    unorderedList.removeChild(unorderedList.lastChild);
  }

  for (let i = 0; i < array.length; i++) {;
    let li = document.createElement("li")
    li.innerText = array[i];
    unorderedList.appendChild(li);
  }
 
}

