// Create a program to prompt the user for hours worked per week and rate per hour 
// and then calculate and display their weekly, monthly, and annual gross pay (hours * rate). 
// Base monthly and annual calculations on 12 months per year and 52 weeks per year.
// ref: https://stackoverflow.com/questions/11563638/how-do-i-get-the-value-of-text-input-field-using-javascript
document.getElementById('activity1').onclick = activity1Handler;

function activity1Handler() {

    var hours = document.getElementById('hours').value;
    var rate = document.getElementById('wage').value;
    var week = getWeek(rate, hours);
    var year = getYear(week);
    var month = getMonth(year);
    displayResult(week, month, year)
}

function getHours() {
    var hours = prompt("How many hours do you work in a week?");
    return hours;
}

function getRate() {
    var rate = prompt("How much are you paid per hour?");
    return rate;
}

function getWeek(rate, hours) {
    var week = hours * rate;
    return week;
}

function getYear(week) {
    var year = week * 52;
    return year;
}

function getMonth(year) {
    var month = year / 12;
    return month;
}

function displayResult(week, month, year) {
    var m = month.toFixed(0);
    var activity1Results = week + " dollars per week, " + m + " dollars per month, " + year + " dollars per year."
    document.getElementById("activity1Results").innerHTML = activity1Results;
}