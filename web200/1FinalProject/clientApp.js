//Refrences:
//https://www.w3schools.com/howto/howto_js_toggle_hide_show.asp
//https://stackoverflow.com/questions/711536/javascript-define-a-variable-if-it-doesnt-exist
//https://www.w3schools.com/jsref/prop_checkbox_checked.asp'//https://love2dev.com/blog/javascript-remove-from-array/


window.addEventListener("load", function () {
  let elements = document.getElementsByTagName("input");

  for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener("focus", inputFocus);
    elements[i].addEventListener("input", inputInput);
  }

  let pizzaSize = document.getElementById("size");
  pizzaSize.addEventListener("input", basePriceSetter);

  let checkboxes = document.getElementsByClassName("checkbox");
  for (let i = 0; i < checkboxes.length; i++) {
    checkboxes[i].addEventListener("input", priceModifier);
  }

  let addPizza = document.getElementById("addPizza");
  addPizza.addEventListener("click", proccessPizza);


  /*  document.getElementById("get").addEventListener("click", getClick);
   document.getElementById("post").addEventListener("click", postClick); */
  document.getElementById("name").focus();



});



function basePriceSetter() {
  let pizzaSize = document.getElementById("size").value;
  if (pizzaSize == "small") {
    var basePrice = 10;
  } else if (pizzaSize == "medium") {
    var basePrice = 15;
  } else if (pizzaSize == "large") {
    var basePrice = 20;
  }
  displayBasePrice(basePrice);
}

function proccessPizza() {
  var customerName = document.getElementById("name").value;
  var phone = document.getElementById("phone").value;
  var address = document.getElementById("address").value;
  var size = document.getElementById("size").value;
  let price = document.getElementById("price").innerText;
  price = price.substring(1);
  price = Number(price);

  checkRequireds(customerName, phone, address, size);
  if (checkRequireds) {
    var toppings = readToppingChecks();
    if (typeof order === "undefined") {

      createOrder(customerName, phone, address, size, toppings, price);

    } else {
      addToOrder(size, toppings, price, order);
    }
    //else addToOrder(size, toppings, price, order);
  }
}


function checkRequireds(customerName, phone, address, size) {
  if (customerName != "" && phone != "" && address != "" && size != "") {
    return true;
  } else alert("All fields except toppings required");
}


function createOrder(customerName, phone, address, size, toppings, price) {
  var pizzas = [Array(size, toppings, price)];
  order = {
    customer: {
      name: customerName,
      phone: phone,
      address: address
    },
    pizzas: pizzas,
    total: price
  }
  displayOrder(order);
  let submitOrder = document.getElementById("submitOrder");
  submitOrder.addEventListener('click', function () {
      proccessOrder(order);
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "receive.php")
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.send(jsonOrder);
    },
    false
  );
  return order;
}

function addToOrder(size, toppings, price, order) {
  order.pizzas.push(Array(size, toppings, price));
  order.total = order.total + price;
  displayOrder(order);
  return order;
}

function inputFocus() {
  document.activeElement.select();
  displayPrompt();
  //checkButtons();
}

function displayPrompt(id = null) {
  const prompts = {
    address: "Enter your full street address",
    name: "Enter your full name",
    phone: "Enter your 10 digit phone number (xxx-xxx-xxxx)"
  }

  let elements = document.getElementsByTagName("output");

  if (id == null) {
    id = document.activeElement.id;

    for (let i = 0; i < elements.length; i++) {
      elements[i].innerText = "";
    }
  }

  try {
    document.getElementById(id + "-prompt").innerText = prompts[id];
  } catch {
    // ignore when the active element is a button
  }
}

function inputInput() {
  let element = document.activeElement;
  let id = element.id;

  if (id == "phone") {
    checkphone();
  }

  if (!element.checkValidity()) {
    document.getElementById(id + "-prompt").innerText = element.validationMessage;
  } else {
    document.getElementById(id + "-prompt").innerText = "";
  }

  //checkButtons();
}

function checkphone() {
  let element = document.getElementById("phone")
  let value = element.value;

  if (value.length > 3 && value.substr(3, 1) != "-") {
    value = value.substr(0, 3) + "-" + value.substr(3);
  }

  if (value.length > 7 && value.substr(7, 1) != "-") {
    value = value.substr(0, 7) + "-" + value.substr(7);
  }

  if (element.value != value) {
    element.value = value;
  }
}

function displayBasePrice(basePrice) {
  document.getElementById("price").innerHTML = "$" + basePrice + ".00";
}

function priceModifier() {
  let price = document.getElementById("price").innerText;
  price = price.substring(1);
  price = Number(price);
  if (price != "" && price != null) {

    let checkboxes = document.getElementsByClassName("checkbox");
    let boxes = [];
    for (let i = 0; i < checkboxes.length; i++) {
      let box = checkboxes[i];
      if (box.checked == true) {
        boxes.push(box);
      }

    }
    let pizzaSize = document.getElementById("size").value;
    if (pizzaSize == "small") {
      price = 10;
    } else if (pizzaSize == "medium") {
      price = 15;
    } else if (pizzaSize == "large") {
      price = 20;
    }
    price = price + boxes.length;
    displaynewPrice(price);
  }
}

function displaynewPrice(price) {
  document.getElementById("price").innerHTML = "$" + price + ".00";
}

function readToppingChecks() {
  var toppingChecks = [];
  var toppings = [];
  var toppingNames = ["sausage", "pepparoni", "bacon", "onion", "mushroom", "bell pepper"];
  let sausageCheck = document.getElementById("sausageCheck").checked;
  toppingChecks.push(sausageCheck);
  let pepparoniCheck = document.getElementById("pepparoniCheck").checked;
  toppingChecks.push(pepparoniCheck);
  let baconCheck = document.getElementById("baconCheck").checked;
  toppingChecks.push(baconCheck);
  let onionCheck = document.getElementById("onionCheck").checked;
  toppingChecks.push(onionCheck);
  let mushroomCheck = document.getElementById("mushroomCheck").checked;
  toppingChecks.push(mushroomCheck);
  let bellPepperCheck = document.getElementById("bellPepperCheck").checked;
  toppingChecks.push(bellPepperCheck);
  for (var i = 0; i < toppingChecks.length; i++) {
    if (toppingChecks[i] == true) {
      toppings.push(toppingNames[i]);
    }
  }
  return toppings;
}

function displayOrder(order) {
  let result = "";
  let pizzas = order.pizzas;
  for (let i = 0; i < pizzas.length; i++) {
    let pizza = pizzas[i];
    let pizzaSize = pizza[0];
    let toppings = pizza[1];
    let price = pizza[2];
    result += pizzaSize + " - " + toppings + " - $" + price + "  |  ";
  }
  let subtotal = order.total

  subtotalDisplay = "SUBTOTAL: $" + subtotal;
  let tax = subtotal * .10;
  let total = subtotal + tax;
  total = total.toFixed(2);
  tax = tax.toFixed(2);
  subtotal = subtotal.toFixed(2);


  document.getElementById("pizzas").innerText = result;
  document.getElementById("subtotal").innerText = "SUBTOTAL: $" + subtotal;
  document.getElementById("tax").innerText = "TAX: $" + tax;
  document.getElementById("total").innerText = "TOTAL: $" + total;
}

function proccessOrder(order) {
  order.instructions = document.getElementById("instructions").value;
  var jsonOrder = JSON.stringify(order);
  const xhr = new XMLHttpRequest();
  xhr.open("POST", "reveive.php");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(jsonOrder);


  console.log(order);

  document.getElementById("output").innerHTML = "Thank you for your order " + order.customer.name + "! We will bring it to " + order.customer.address + " soon, and we'll be sure to follow your special instuction: " + order.instructions + " :)"
}